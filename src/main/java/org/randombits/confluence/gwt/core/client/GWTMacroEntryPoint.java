package org.randombits.confluence.gwt.core.client;

import org.randombits.confluence.gwt.core.client.params.MacroParams;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * This is an abstract implementation of the GWT {@link EntryPoint} which does
 * some of the extra work required to make GWT work in a Confluence Macro
 * context.
 * 
 * @author David Peterson
 */
public abstract class GWTMacroEntryPoint implements EntryPoint {

    private static final String PANEL = ":Panel";

    public void onModuleLoad() {

        int i = 0;
        RootPanel panel = getRootPanel( i );
        while ( panel != null ) {
            MacroParams macroParams = MacroParams.get( getModuleName(), i );

            try {
                onMacroLoad( panel, macroParams );
            } catch ( ConfigurationException e ) {
                e.printStackTrace();
                panel.clear();
                panel.add( new ErrorPanel( e ) );
            }

            // Next element.
            i++;
            panel = getRootPanel( i );
        }
    }

    private GWTModule module;

    public GWTMacroEntryPoint( GWTModule module ) {
        this.module = module;
    }

    /**
     * This method is called for each macro instance on a page.
     * 
     * @param rootPanel
     * @param params
     */
    protected abstract void onMacroLoad( RootPanel rootPanel, MacroParams params ) throws ConfigurationException;

    protected GWTModule getModule() {
        return module;
    }

    /**
     * Returns the module name for this entry point to use for HTML elements and
     * other values which need to be uniquely identified with this macro.
     * 
     * @return
     */
    protected String getModuleName() {
        return module.getFullName();
    }

    private RootPanel getRootPanel( int i ) {
        return RootPanel.get( getModuleName() + i + PANEL );
    }
}
