package org.randombits.confluence.gwt.core.client;

public abstract class GWTModule {
    private String name;

    private String pluginKey;

    public GWTModule( String module, String pluginKey ) {
        this.name = module;
        this.pluginKey = pluginKey;
    }

    public String getName() {
        return name;
    }

    public String getPluginKey() {
        return pluginKey;
    }
    
    public String getFullName() {
        return pluginKey + "." + name;
    }
}
