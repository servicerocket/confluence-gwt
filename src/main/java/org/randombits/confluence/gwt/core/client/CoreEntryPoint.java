package org.randombits.confluence.gwt.core.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * This is a non-functional class purely to allow the GWT to be compiled
 * for confirmation during building, since all modules require an entry point.
 */
final class CoreEntryPoint implements EntryPoint {

    private CoreEntryPoint() {
    }
    
    public void onModuleLoad() {
        // Do nothing.
    }

}
