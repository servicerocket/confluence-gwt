package org.randombits.confluence.gwt.core.server;

import org.randombits.confluence.gwt.core.client.GWTModule;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.plugin.webresource.WebResourceManager;

public abstract class GWTActionSupport extends ConfluenceActionSupport {
    private static final String GWT_PAGE = "gwt-page";

    private WebResourceManager webResourceManager;

    private GWTModule module;

    private String gwtScriptPath;

    public GWTActionSupport( GWTModule module ) {
        this.module = module;
    }

    public GWTModule getModule() {
        return module;
    }
    
    /**
     * Returns the name of a plugin module to use when retrieving resources. Often this will
     * be the XWork or Conveyor action configuration module.
     */
    protected abstract String getPluginModule();

    public void setWebResourceManager( WebResourceManager webResourceManager ) {
        this.webResourceManager = webResourceManager;
    }
    
    public String execute() {
        gwtScriptPath = findGwtScriptPath();
        if ( gwtScriptPath != null )
            return GWT_PAGE;
        
        return ERROR;
    }
    
    public abstract String getTitle();
    
    public String getGwtScriptPath() {
        return gwtScriptPath;
    }

    private String findGwtScriptPath() {
        GWTModule module = getModule();

        String resourceName = "gwt/" + module.getName() + "/" + module.getFullName() + ".nocache.js";
        String resourceKey = module.getPluginKey() + ":" + getPluginModule();

        try {
            return webResourceManager.getStaticPluginResource( resourceKey, resourceName );
        } catch ( NullPointerException e ) {
            System.err.println( "Unable to locate javascript for GWT: " + resourceKey + " - " + resourceName );
            e.printStackTrace();
            return null;
        }
    }
}
