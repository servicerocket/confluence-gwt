package org.randombits.confluence.gwt.core.server;

import java.util.Iterator;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.spring.container.ContainerManager;

public final class CoreUtils {
    private static String pluginKey;

    private static PluginAccessor pluginAccessor;

    private static PluginAccessor getPluginAccessor() {
        if ( pluginAccessor == null )
            pluginAccessor = ( PluginAccessor ) ContainerManager.getComponent( "pluginAccessor" );
        return pluginAccessor;
    }

    /**
     * Returns the key for the plugin the current class is a member of.
     * 
     * @return The plugin key, or <code>null</code> if this class is not
     *         uploaded.
     */
    public static String getPluginKey() {
        if ( pluginKey == null ) {
            Iterator i = getPluginAccessor().getEnabledPlugins().iterator();
            ClassLoader loader = CoreUtils.class.getClassLoader();
            while ( pluginKey == null && i.hasNext() ) {
                Plugin plugin = ( Plugin ) i.next();
                Class clazz;
                try {
                    clazz = plugin.loadClass( CoreUtils.class.getName(), Object.class );
                    if ( clazz != null && clazz.getClassLoader() == loader )
                        pluginKey = plugin.getKey();
                } catch ( ClassNotFoundException e ) {
                    // Do nothing.
                }
            }
        }
        return pluginKey;
    }

    private CoreUtils() {
    }
}
