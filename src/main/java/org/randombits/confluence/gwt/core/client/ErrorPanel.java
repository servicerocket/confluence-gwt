package org.randombits.confluence.gwt.core.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ErrorPanel extends Composite {
    private Label heading;

    private Label message;
    
    public ErrorPanel( Exception e ) {
        this( e, null );
    }

    public ErrorPanel( Exception e, String heading ) {
        this( e.getMessage(), heading );
    }
    
    public ErrorPanel( String message ) {
        this( message, null );
    }

    public ErrorPanel( String message, String heading ) {
        VerticalPanel panel = new VerticalPanel();
        
        if ( this.heading != null ) {
            this.heading = new Label( heading );
            this.heading.setStyleName( "confluence-ErrorHeading" );
            panel.add( this.heading );
        }
        
        this.message = new Label( message );
        this.message.setStyleName( "confluence-ErrorMessage" );
        panel.add( this.message );
        
        initWidget( panel );
        
        setStyleName( "confluence-ErrorPanel" );
    }
}
