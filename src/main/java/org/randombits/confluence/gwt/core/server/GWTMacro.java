package org.randombits.confluence.gwt.core.server;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.randombits.confluence.gwt.core.client.GWTModule;
import org.randombits.confluence.gwt.core.client.params.ContextParams;
import org.randombits.confluence.gwt.core.client.params.GlobalParams;
import org.randombits.confluence.support.ConfluenceMacro;
import org.randombits.confluence.support.MacroInfo;
import org.randombits.storage.IndexedStorage;
import org.randombits.storage.MapStorage;
import org.randombits.storage.Storage;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;

public abstract class GWTMacro extends ConfluenceMacro {

    private static final String GLOBAL_PARAMS = GlobalParams.GLOBAL_PARAMS;

    private static final String MACRO_COUNT = ":Count";

    private static final String CONTEXT_ID = "net.customware.confluence.gwt.core.server:ContextId";

    private static final String RAW = ": = | RAW | = :";

    private static final String VELOCITY_TEMPLATE = "org/randombits/confluence/gwt/core/server/macro.vm";

    private static final Logger LOG = Logger.getLogger( GWTMacro.class );

    private SettingsManager settingsManager;

    private WebResourceManager webResourceManager;

    private GWTModule module;

    private LocaleManager localeManager;

    public GWTMacro( GWTModule module ) {
        this.module = module;
    }

    public GWTModule getModule() {
        return module;
    }

    protected WebResourceManager getWebResourceManager() {
        return webResourceManager;
    }

    private void appendParam( StringBuffer out, String key, String value, boolean addComma ) {
        out.append( '"' ).append( StringEscapeUtils.escapeJavaScript( key ) ).append( "\":" );
        if ( value == null )
            out.append( "null" );
        else
            out.append( '"' ).append( StringEscapeUtils.escapeJavaScript( value ) ).append( '"' );
        if ( addComma )
            out.append( ',' );
    }

    /**
     * This method returns XHTML to be displayed on the final page.
     */
    public String execute( MacroInfo info ) throws MacroException {

        validateMacro( info );

        StringBuffer out = new StringBuffer();

        // Generate the once-per-request content
        out.append( generateGlobalXHTML( info ) );

        int contextId = getContextId( info );
        if ( contextId == -1 )
            throw new MacroException( "This macro cannot function in this context." );

        // Generate the once-per-context content.
        out.append( generateContextXHTML( info, contextId ) );

        // Figure out the macro count.
        int macroCount = getMacroCount( info );

        if ( macroCount == 0 ) {
            out.append( generatePageXHTML( info ) );
        }

        String macroXHTML;

        macroXHTML = generateMacroXHTML( info, contextId, macroCount );

        out.append( macroXHTML );

        return out.toString();
    }

    private int getContextId( MacroInfo info ) {
        Storage reqAttrs = info.getRequestAttributes();
        Storage ctxParams = info.getRenderContextParams();
        if ( reqAttrs == null || ctxParams == null )
            return -1;

        int contextId = reqAttrs.getInteger( CONTEXT_ID, -1 );
        int currentId = ctxParams.getInteger( CONTEXT_ID, -1 );

        if ( contextId == -1 || currentId != contextId ) {
            reqAttrs.setInteger( CONTEXT_ID, ++contextId );
        }

        return contextId;
    }

    /**
     * This method is called before the
     * {@link #generateMacroXHTML(MacroInfo, int)} method, and can be used to
     * check that the macro parameters/body/whatever are correct. If there is a
     * problem, throw {@link MacroException}.
     * 
     * @param info
     * @throws MacroException
     */
    protected abstract void validateMacro( MacroInfo info ) throws MacroException;

    /**
     * This method is called once per request across all GWTMacro subclasses. It
     * should contain XHTML which should only be output once on the whole
     * request.
     * 
     * @param info
     *            The macro info.
     * @return The XHTML.
     */
    private String generateGlobalXHTML( MacroInfo info ) {
        Storage reqAttrs = info.getRequestAttributes();

        // Check if we've already done the global values.
        if ( reqAttrs.getBoolean( GLOBAL_PARAMS, false ) )
            return "";

        // Get the global parameter list.
        Storage globalParams = new MapStorage( new java.util.HashMap() );
        initGlobalParams( info, globalParams );

        StringBuffer out = new StringBuffer();

        if ( globalParams.nameSet().size() > 0 ) {
            out.append( "<span id='" ).append( GLOBAL_PARAMS ).append( "' style='display:none'>" );

            appendParams( out, globalParams );

            out.append( "</span>" );
        }

        // Output the current locale for GWT macros to pick up.
        Locale locale = localeManager.getLocale( info.getCurrentUser() );
        if ( locale != null ) {
            out.append( "<meta name='gwt:property' content='locale=" + locale + "'/>" );
        }

        reqAttrs.setBoolean( GLOBAL_PARAMS, true );

        return out.toString();
    }

    private void appendParams( StringBuffer out, Storage params ) {
        StringBuffer paramsOut = new StringBuffer();
        paramsOut.append( "{" );

        Iterator i = params.nameSet().iterator();
        while ( i.hasNext() ) {
            String name = ( String ) i.next();
            Object value = params.getObject( name, null );
            appendParam( paramsOut, name, value == null ? null : value.toString(), i.hasNext() );
        }

        paramsOut.append( "}" );

        out.append( StringEscapeUtils.escapeHtml( paramsOut.toString() ) );
    }

    /**
     * Override this method to add extra global parameters. Make sure that
     * <code>super.initGlobalParams( info, globalParams )</code> is called.
     * 
     * @param info
     *            The macro info.
     * @param globalParams
     *            The global parameters.
     */
    protected void initGlobalParams( MacroInfo info, Storage globalParams ) {
        // Append HTTP Request information.
        HttpServletRequest req = info.getRequest();
        if ( req != null )
            globalParams.setString( GlobalParams.CONTEXT_PATH, req.getContextPath() );

        User user = info.getCurrentUser();
        if ( user != null ) {
            globalParams.setString( GlobalParams.USERNAME, user.getName() );
        }

        // Lastly, append the baseUrl.
        Settings globalSettings = getGlobalSettings();
        globalParams.setString( GlobalParams.BASE_URL, globalSettings.getBaseUrl() );
    }

    /**
     * This method is called once per page across all GWTMacro subclasses. It
     * should contain XHTML which should only be output once on the whole
     * request.
     * 
     * @param info
     *            The macro info.
     * @return The XHTML.
     */
    private String generateContextXHTML( MacroInfo info, int contextCount ) {
        Storage ctxParams = info.getRenderContextParams();

        // Check if we've already done this context's values.
        if ( ctxParams.getInteger( CONTEXT_ID, -1 ) >= 0 )
            return "";

        ctxParams.setInteger( CONTEXT_ID, contextCount );

        Storage contextParams = new MapStorage( new java.util.HashMap() );
        initContextParams( info, contextParams );

        // Now, output the values.
        StringBuffer params = new StringBuffer();
        params.append( "<span id='" ).append( ContextParams.CONTEXT_PARAMS ).append( contextCount ).append(
                "' style='display:none'>" );

        appendParams( params, contextParams );

        params.append( "</span>" );

        return params.toString();
    }

    protected void initContextParams( MacroInfo info, Storage contextParams ) {

        contextParams.setString( ContextParams.OUTPUT_TYPE, info.getPageContext().getOutputType() );

        if ( info.getSpace() != null ) {
            contextParams.setString( ContextParams.SPACE_KEY, info.getSpace().getKey() );
        }

        ContentEntityObject content = info.getContent();
        if ( content != null ) {
            contextParams.setString( ContextParams.CONTENT_ID, content.getIdAsString() );
            contextParams.setString( ContextParams.CONTENT_TYPE, content.getType() );
        }

    }

    private Settings getGlobalSettings() {
        return settingsManager.getGlobalSettings();
    }

    private int getMacroCount( MacroInfo info ) {
        Storage storage = info.getRequestAttributes();
        if ( storage == null )
            return -1;

        int count = storage.getInteger( getModuleName() + MACRO_COUNT, 0 );
        storage.setInteger( getModuleName() + MACRO_COUNT, count + 1 );
        return count;
    }

    /**
     * Returns the GWT Module this macro works with.
     * 
     * @return
     */
    protected String getModuleName() {
        return module.getFullName();
    }

    /**
     * The key for the plugin the macro is in.
     * 
     * @return The plugin key.
     */
    protected String getPluginKey() {
        return module.getPluginKey();
    }

    public void setSettingsManager( SettingsManager settingsManager ) {
        this.settingsManager = settingsManager;
    }

    /**
     * This method is called once per page, per macro. Any macro-specific code
     * which should be output only once on the page should go here.
     * 
     * @param info
     *            The macro info.
     * @return The page-wide macro code.
     */
    protected String generatePageXHTML( MacroInfo info ) throws MacroException {
        if ( webResourceManager == null )
            throw new MacroException( "WebResourceManager is missing." );

        GWTModule module = getModule();
        // String resourceName = "js/" + moduleName + "/" + moduleName +
        // ".nocache.js";
        String resourceName = "gwt/" + module.getName() + "/" + module.getFullName() + ".nocache.js";
        String resourceKey = module.getPluginKey() + ":" + getMacroName();

        try {
            String src = webResourceManager.getStaticPluginResource( resourceKey, resourceName );

            StringBuffer out = new StringBuffer();
            out.append( "<script type='text/javascript' language='javascript'" );
            out.append( " src='" ).append( src ).append( "'>" );
            out.append( "</script>" );

            return out.toString();
        } catch ( NullPointerException e ) {
            LOG.error( "Unable to locate javascript for GWT: " + resourceKey + " - " + resourceName, e );
            System.err.println( "Unable to locate javascript for GWT: " + resourceKey + " - " + resourceName );
            e.printStackTrace();
            throw new MacroException( "Unable to locate javascript with the following key: " + resourceKey, e );
        }
    }

    public void setWebResourceManager( WebResourceManager webResourceManager ) {
        this.webResourceManager = webResourceManager;
    }

    /**
     * The name of this macro.
     * 
     * @return The macro name.
     */
    protected abstract String getMacroName();

    public void setLocaleManager( LocaleManager localeManager ) {
        this.localeManager = localeManager;
    }

    protected String generateMacroXHTML( MacroInfo info, int contextId, int macroCount ) throws MacroException {
        // Convert the macro params into a JSON string.
        String macroParams = generateMacroParams( info.getMacroParams() );

        // Render the velocity template.
        try {
            Map contextMap = MacroUtils.defaultVelocityContext();
            contextMap.put( "moduleName", getModuleName() );
            contextMap.put( "pluginKey", getPluginKey() );
            contextMap.put( "contextId", contextId );
            contextMap.put( "macroCount", macroCount );
            contextMap.put( "macroParams", StringEscapeUtils.escapeHtml( macroParams ) );
            contextMap.put( "macroBody", info.getMacroBody() );
            return VelocityUtils.getRenderedTemplate( getVelocityTemplate(), contextMap );
        } catch ( Exception e ) {
            LOG.error( "Error!", e );
            e.printStackTrace();
            throw new MacroException( e.getMessage(), e );
        }
    }

    /**
     * Returns the name of the velocity template to execute for this macro. By
     * default, renders
     * <code>"org/randombits/confluence/gwt/core/server/SimpleGTWMacro.vm"</code>.
     * May be overridden to have an alternate template rendered.
     * 
     * @return The velocity template path.
     */
    protected String getVelocityTemplate() {
        return VELOCITY_TEMPLATE;
    }

    private String generateMacroParams( IndexedStorage storage ) {
        StringBuffer out = new StringBuffer();

        out.append( "{" );
        Iterator i = storage.nameSet().iterator();
        while ( i.hasNext() ) {
            String key = ( String ) i.next();
            if ( !RAW.equals( key ) ) {
                out.append( '"' ).append( StringEscapeUtils.escapeJavaScript( key ) )
                        .append( '"' );
                out.append( ':' );
                out.append( '"' ).append( StringEscapeUtils.escapeJavaScript( storage.getString( key, "" ) ) )
                        .append( '"' );
                if ( i.hasNext() )
                    out.append( ',' );
            }
        }
        out.append( "}" );

        return out.toString();
    }

}