package org.randombits.confluence.gwt.core.client.params;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

public final class MacroParams extends Params {

    private static final String PARAMS = ":Params";

    public static MacroParams get( String moduleName, int macroNumber ) {
        JSONObject params = null;
        String contextId = null;
        Element paramsElement = DOM.getElementById( moduleName + macroNumber + PARAMS );

        if ( paramsElement != null ) {
            String jsonString = DOM.getInnerText( paramsElement );
            if ( jsonString != null && !"".equals( jsonString.trim() ) ) {
                params = ( JSONObject ) JSONParser.parse( jsonString );
            }
            contextId = DOM.getElementAttribute( paramsElement, "contextId" );
        }

        if ( params == null )
            params = new JSONObject();

        return new MacroParams( params, macroNumber, contextId );
    }

    private String contextId;

    private ContextParams contextParams;

    private int macroNumber;

    private MacroParams( JSONObject params, int macroNumber, String contextId ) {
        super( params );
        this.macroNumber = macroNumber;
        this.contextId = contextId;
    }

    /**
     * Returns the count value of this macro. This is the number of this macro
     * in the current page.
     * 
     * @return the macro number.
     */
    public int getMacroNumber() {
        return macroNumber;
    }

    public ContextParams getContextParams() {
        if ( contextParams == null && contextId != null )
            contextParams = ContextParams.get( contextId );
        return contextParams;
    }

    public String getString( int index, String defaultValue ) {
        return getString( String.valueOf( index ), defaultValue );
    }

    public long getLong( int index, long defaultValue ) {
        return getLong( String.valueOf( index ), defaultValue );
    }

    public boolean getBoolean( int index, boolean defaultValue ) {
        return getBoolean( String.valueOf( index ), defaultValue );
    }

    public double getNumber( int index, double defaultValue ) {
        return getNumber( String.valueOf( index ), defaultValue );
    }

}
