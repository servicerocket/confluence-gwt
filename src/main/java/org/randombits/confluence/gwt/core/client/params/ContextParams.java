package org.randombits.confluence.gwt.core.client.params;

import java.util.Map;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

public final class ContextParams extends Params {

    public static final String OUTPUT_TYPE = "outputType";

    public static final String SPACE_KEY = "spaceKey";

    public static final String CONTENT_ID = "contentId";

    public static final String CONTENT_TYPE = "contentType";

    public static final String CONTEXT_PARAMS = "org.randombits.confluence.gwt.client.ContextParams:";

    private static Map contexts;

    public static ContextParams get( String id ) {
        ContextParams contextParams = null;

        if ( contexts == null )
            contexts = new java.util.HashMap();
        else
            contextParams = ( ContextParams ) contexts.get( id );

        if ( contextParams == null ) {
            JSONObject params = null;
            Element element = DOM.getElementById( CONTEXT_PARAMS + id );
            if ( element != null )
                params = ( JSONObject ) JSONParser.parse( DOM.getInnerText( element ) );
            else
                params = new JSONObject();

            contextParams = new ContextParams( params );
            contexts.put( id, contextParams );
        }

        return contextParams;
    }

    private ContextParams( JSONObject params ) {
        super( params );
    }

    public String getOutputType( String defaultValue ) {
        return getString( OUTPUT_TYPE, defaultValue );
    }

    public String getSpaceKey( String defaultValue ) {
        return getString( SPACE_KEY, defaultValue );
    }

    public long getContentId( long defaultValue ) {
        return getLong( CONTENT_ID, defaultValue );
    }

    public String getContentType( String defaultValue ) {
        return getString( CONTENT_TYPE, defaultValue );
    }

}
