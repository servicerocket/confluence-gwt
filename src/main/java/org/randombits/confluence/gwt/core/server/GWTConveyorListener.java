package org.randombits.confluence.gwt.core.server;

import org.randombits.confluence.conveyor.AbstractConveyorListener;
import org.randombits.confluence.conveyor.config.ConveyorConfigurationProvider;

public class GWTConveyorListener extends AbstractConveyorListener {

    /**
     * Creates both a GWT provider and the standard 'conveyor-config.xml' provider.
     */
    protected ConveyorConfigurationProvider[] createProviders() {
        return new ConveyorConfigurationProvider[]{new ConveyorConfigurationProvider( "gwt-conveyor-config.xml" ),
                new ConveyorConfigurationProvider()};
    }

}
