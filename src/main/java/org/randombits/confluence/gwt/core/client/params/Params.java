package org.randombits.confluence.gwt.core.client.params;

import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

public abstract class Params {

    private JSONObject params;

    public Params( JSONObject params ) {
        this.params = params;
    }

    public String getString( String key, String defaultValue ) {
        JSONValue value = params.get( key );
        if ( value == null || value.isNull() != null )
            return defaultValue;

        JSONString stringValue = value.isString();
        if ( stringValue != null )
            return stringValue.stringValue();

        JSONNumber numberValue = value.isNumber();
        if ( numberValue != null )
            return String.valueOf( numberValue.getValue() );

        JSONBoolean booleanValue = value.isBoolean();
        if ( booleanValue != null )
            return String.valueOf( booleanValue.booleanValue() );

        return defaultValue;
    }

    public boolean getBoolean( String key, boolean defaultValue ) {
        JSONValue value = params.get( key );
        if ( value == null || value.isNull() != null )
            return defaultValue;

        JSONBoolean booleanValue = value.isBoolean();
        if ( booleanValue != null )
            return booleanValue.booleanValue();

        JSONString stringValue = value.isString();
        if ( stringValue != null )
            return "true".equalsIgnoreCase( stringValue.stringValue() );

        return defaultValue;
    }

    public double getNumber( String key, double defaultValue ) {
        JSONValue value = params.get( key );
        if ( value == null || value.isNull() != null )
            return defaultValue;

        JSONNumber numberValue = value.isNumber();
        if ( numberValue != null )
            return numberValue.getValue();

        JSONString stringValue = value.isString();
        if ( stringValue != null )
            try {
                return Double.parseDouble( stringValue.stringValue() );
            } catch ( NumberFormatException e ) {
                return defaultValue;
            }

        return defaultValue;
    }

    public long getLong( String key, long defaultValue ) {
        return ( long ) getNumber( key, defaultValue );
    }

}
