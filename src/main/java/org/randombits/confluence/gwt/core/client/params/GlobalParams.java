package org.randombits.confluence.gwt.core.client.params;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

public final class GlobalParams extends Params {
    public static final String GLOBAL_PARAMS = "com.google.gwt.GWT:Params";

    public static final String CONTEXT_PATH = "contextPath";

    public static final String USERNAME = "username";

    public static final String BASE_URL = "baseUrl";

    private static GlobalParams globalParams = null;

    public static GlobalParams get() {

        if ( globalParams == null ) {
            JSONObject params = null;
            Element element = DOM.getElementById( GLOBAL_PARAMS );
            if ( element != null )
                params = ( JSONObject ) JSONParser.parse( DOM.getInnerText( element ) );
            else
                params = new JSONObject();

            globalParams = new GlobalParams( params );
        }
        return globalParams;
    }

    private GlobalParams( JSONObject params ) {
        super( params );
    }

    public String getContextPath( String defaultValue ) {
        return getString( CONTEXT_PATH, defaultValue );
    }
    
    public String getUsername( String defaultValue ) {
        return getString( USERNAME, defaultValue );
    }
    
    public String getBaseURL( String defaultValue ) {
        return getString( BASE_URL, defaultValue );
    }

}
